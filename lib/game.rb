require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
attr_accessor :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @player_one.mark = :X
    @player_two.mark = :O
    @current_player = @player_one
  end

  def play_turn
    puts "It's #{@current_player.name}\'s turn: "
    @current_player.display(@board)
    new_move = @current_player.get_move
    @board.place_mark(new_move, @current_player.mark)
    puts "#{@current_player.name} placed an #{@current_player.mark} at #{new_move}: "
    @current_player.display(@board)
    switch_players!
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    elsif @current_player == @player_two
      @current_player = @player_one
    end
  end

  def play
    until @board.over?
      play_turn
    end
    puts "Game over!"
    puts "#{@player_one.name} wins!" if @board.winner == :X
    puts "#{@player_two.name} wins!" if @board.winner == :O
  end
end

if __FILE__ == $PROGRAM_NAME
  player_one = HumanPlayer.new("Omar")
  player_two = ComputerPlayer.new("AI")
  tic_tac = Game.new(player_one, player_two)
  tic_tac.play
end
