require 'byebug'

class ComputerPlayer
  attr_accessor :mark
  attr_reader :board, :name

  def initialize(name)
    @name = name
    @board = nil
    @mark = nil
  end

  def display(board)
    @board = board
    @board.render
  end

  def get_move
    # check for winning move
    return winning_move unless winning_move == nil
    # if we get here, there is no winning move, so we make a random move
    random_move
  end

  private
  def winning_move
    final_move = nil
    temp_board = @board
    @board.grid.each_with_index do |row, row_index|
      row.each_with_index do |col, col_index|
        temp_pos = [row_index, col_index]
        if temp_board.empty?(temp_pos)
          temp_board[temp_pos] = @mark
          final_move = temp_pos if temp_board.winning_symbol?(@mark)
          temp_board[temp_pos] = nil
        end
      end
    end

    final_move
  end

  private
  def random_move
    while true
      rand_row = rand(0 ... 3)
      rand_col = rand(0 ... 3)
      pos = [rand_row, rand_col]
      return pos if @board.empty?(pos)
    end
  end
end
