class Board
  attr_accessor :grid

  def initialize(grid = nil)
    if grid
      @grid = grid
    else
      @grid = [
        [nil, nil, nil],
        [nil, nil, nil],
        [nil, nil, nil]
      ]
    end
  end

  def [](pos) # e.g. pos = [0, 1]
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark) # e.g. pos => [0, 1], mark => :X
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark) # e.g. pos => [0, 2], mark => :X
    if empty?(pos)
      self[pos] = mark
    else
      raise "That position is not empty!"
    end
  end

  def empty?(pos)
    return true if self[pos] == nil
    false
  end

  def winner
    if winning_symbol?(:X) && !winning_symbol?(:O)
      return :X
    elsif winning_symbol?(:O) && !winning_symbol?(:X)
      return :O
    else
      return nil
    end
  end

  def winning_symbol?(sym)
    return true if row_winner?(sym)
    return true if col_winner?(sym)
    return true if diag_winner?(sym)
    false
  end

  def row_winner?(sym)
    @grid.each do |row|
      return true if row.all? { |el| el == sym }
    end
    false
  end

  def col_winner?(sym)
    (0..2).each do |index|
      return true if @grid.all? { |row| row[index] == sym }
    end
    false
  end

  def diag_winner?(sym)
    return true if @grid[0][0] == sym && @grid[1][1] == sym && @grid[2][2] == sym
    return true if @grid[0][2] == sym && @grid[1][1] == sym && @grid[2][0] == sym
    false
  end

  def over?
    return true if winner
      return true if @grid.all? { |row| row == row.compact }
    false
  end

  def render
    @grid.each_with_index do |row, row_index|
      puts "#{row[0].to_s}   | #{row[1].to_s}    |    #{row[2].to_s}"
      puts "-------------" unless row_index == 2
    end
  end

end
