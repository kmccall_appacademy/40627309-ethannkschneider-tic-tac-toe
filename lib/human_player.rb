class HumanPlayer
attr_reader :name
attr_accessor :mark

  def initialize(name)
    @name = name
    @board = nil
    @mark = nil
  end

  def get_move
=begin    puts "Pick your move! \n Input the row and column coordinates
    separated by a comma \n Ex: Top left = '0, 0' ; \n Ex: Bottom right  = '2, 2' ;
    \n Ex: Middle = '1, 1' "
=end
    puts "where"
    player_input = gets.chomp
    raise "Input too long!" if player_input.length > 4
    raise "Input too short!" if player_input.length < 4
    raise "Input not formatted correctly!" if !player_input.include?(",")
    player_pos = player_input.split(", ").map { |el| el.to_i }
    puts "you chose #{player_pos}"
    player_pos
  end

  def display(board)
    @board = board
    @board.render
  end
end
